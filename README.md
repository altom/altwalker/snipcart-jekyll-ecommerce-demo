# Static site e-commerce with Snipcart & Jekyll

> **Warning**: **This repository was moved to GitHub, you can find the new repository at: https://github.com/altwalker/jekyll-ecommerce.**


Demo code for an e-commerce static site with Snipcart & Jekyll.

> [Read the full blog post](https://snipcart.com/blog/static-site-e-commerce-part-2-integrating-snipcart-with-jekyll)

> [Check out the live demo site](https://altom.gitlab.io/altwalker/snipcart-jekyll-ecommerce-demo/)

Enjoy folks!
